package main

import (
	"auth/strategy"
	"fmt"
	"net/http"
	"text/template"
	"time"

	"github.com/sirupsen/logrus"
)

func main() {
	http.HandleFunc("/", rootHandler)
	http.HandleFunc("/user", strategy.LoginHandler)    // REDIS_DB1
	http.HandleFunc("/email", strategy.LoginHandler)   // REDIS_DB2
	http.HandleFunc("/phone", strategy.LoginHandler)   // REDIS_DB3
	http.HandleFunc("/hook", strategy.LoginHandler)    // REDIS_DB0
	http.HandleFunc("/refresh", strategy.LoginHandler) // REDIS_DB0
	http.HandleFunc("/valid", strategy.LoginHandler)   // REDIS_DB0
	http.HandleFunc("/token", strategy.LoginHandler)   // REDIS_DB0
	http.HandleFunc("/recovery", strategy.LoginHandler)
	http.HandleFunc("/changepwd", strategy.LoginHandler)
	logrus.WithFields(logrus.Fields{
		"date": time.Now(),
	}).Info("  🌐  🌨️  Server is ready  🌨️  🌐  ")
	err := http.ListenAndServe(":3030", nil)
	if err != nil {
		fmt.Println("Could not start the server", err)
	}
}

func rootHandler(w http.ResponseWriter, r *http.Request) {
	// Cargar el archivo de plantilla HTML
	tpl, err := template.ParseFiles("src/index.api.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Datos a pasar a la plantilla (podrían ser dinámicos)
	data := struct {
		Title string
	}{
		Title: "API DE AUTENTIFICACIÓN BEETMANN",
	}

	// Ejecutar la plantilla y escribir la respuesta HTTP
	w.Header().Set("Content-Type", "text/html")
	err = tpl.Execute(w, data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
