package strategy

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt"
)

type HookAuth struct{}

func (u *HookAuth) Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error) {
	// Decodificar el cuerpo de la solicitud en una estructura definida
	var requestData SolicitudHTTP
	err := json.NewDecoder(r.Body).Decode(&requestData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	// fmt.Println("Query: ", requestData.Request.Query)
	// Verificar que el token tenga el formato correcto (Bearer <token>)
	tokenParts := strings.Split(requestData.Headers.Authorization, " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		http.Error(w, "Formato de token incorrecto", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Obtener el token JWT
	tokenString := tokenParts[1]
	// Parsear y validar el token JWT
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return getKey(), nil
	})
	if err != nil || !token.Valid {
		http.Error(w, "Token JWT inválido", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}
	// Verificar las reclamaciones específicas de Hasura
	hasuraClaims := claims.HTTPSHasuraIoJwtClaims

	var ok bool
	if ok, err = validToken(claims.Storage_Key); !ok || err != nil {
		http.Error(w, "Usuario no autorizado", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Si todo está bien, responder con éxito
	jsonResponse, _ := json.Marshal(hasuraClaims)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // Esto podría moverse fuera del bloque if-else
	w.Write(jsonResponse)
	return true, http.StatusOK, nil
}
