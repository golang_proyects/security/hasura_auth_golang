package strategy

import (
	mssql "auth/singletonmssql"
	"encoding/json"
	"fmt"
	"net/http"
)

// Estrategia de autenticación por nombre de usuario y contraseña
type EmailAuth struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (u *EmailAuth) Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error) {

	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		http.Error(w, "Error al leer la solicitud", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	var dbUserData []UserInfoInput = make([]UserInfoInput, 0)
	var db_redis int = 2

	userData, err := getUserFromCache(db_redis, u.Email)
	if err != nil {

		user, err := mssql.DoQuery("exec [auth_validate_user] @Email, @Password, 3;", []mssql.SqlArgs{
			{Name: "Email", Value: u.Email}, {Name: "Password", Value: u.Password},
		})
		if err != nil {
			// fmt.Println(err)
			http.Error(w, "Error al conectar el servidor con la base de datos", http.StatusBadRequest)
			return false, http.StatusBadRequest, err
		}

		json.Unmarshal(user, &dbUserData)

		if ok, err := setUserFromCache(db_redis, u.Email, user); !ok || err != nil {
			fmt.Println("No se pudo guardar en cache: ", u.Email)
		}

	} else {
		if userData[0].Password != encryptPass(u.Password) || len(userData) <= 0 {
			http.Error(w, "Credenciales erroneas", http.StatusUnauthorized)
			return false, http.StatusUnauthorized, err
		}
		dbUserData = userData
	}

	if len(dbUserData) > 0 {
		tokenString, err := createToken(dbUserData[0])
		if err != nil {
			http.Error(w, "Error de servidor al crear el token", http.StatusInternalServerError)
			return false, http.StatusInternalServerError, err
		}
		var loginResponse LoginResponse = LoginResponse{
			Token:          tokenString,
			UserInfoOutput: dbUserData[0].ToOutput(),
		}

		// Serializar el objeto JSON
		response, err := json.Marshal(loginResponse)
		if err != nil {
			http.Error(w, "Error de servidor", http.StatusInternalServerError)
			return false, http.StatusInternalServerError, err
		}

		// Establecer el encabezado Content-Type como JSON
		w.Header().Set("Content-Type", "application/json")

		// Enviar el token JWT como respuesta
		w.Write(response)

	} else {
		http.Error(w, "Credenciales erroneas", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, err
	}
	return true, http.StatusOK, nil
}
