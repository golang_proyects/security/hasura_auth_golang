package strategy

import (
	mssql "auth/singletonmssql"
	"encoding/json"
	"net/http"
)

// Estrategia de autenticación por nombre de usuario y contraseña
type ChangePwd struct {
	Id           int    `json:"id"`
	Correo       string `json:"correo"`
	Nombre       string `json:"nombre"`
	Celular      string `json:"celular"`
	Password     string `json:"password"`
	New_password string `json:"new_password"`
}

func (u *ChangePwd) GetCorreo() string {
	return u.Correo
}

func (u *ChangePwd) GetNombre() string {
	return u.Nombre
}

func (u *ChangePwd) GetCelular() string {
	return u.Celular
}

func (u *ChangePwd) Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error) {
	err := json.NewDecoder(r.Body).Decode(&u)
	if err != nil {
		http.Error(w, "Error al leer la solicitud", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}
	var response []byte
	response, err = mssql.DoQuery("exec [change_pwd] @Correo, @Password, @NewPassword;", []mssql.SqlArgs{
		{Name: "Correo", Value: u.Correo},
		{Name: "Password", Value: u.Password},
		{Name: "NewPassword", Value: u.New_password},
	})

	if err != nil {
		http.Error(w, "Error al conectar el servidor con la base de datos", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	var result []MssqlResponseInfo
	err = json.Unmarshal(response, &result)
	if err != nil {
		http.Error(w, "Error al obetener respuesta de base de datos", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	if len(result) > 0 {
		if result[0].Affects_rows == 0 {
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			w.Write([]byte(`{
				"affects_rows": 0,
				"id": null,
				"error": "Credenciales erroneas"
			}`))
			return false, http.StatusUnauthorized, err
		}
	} else {
		http.Error(w, "Error al obetener respuesta de base de datos", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	u.Celular = result[0].UserInfo[0].Celular
	u.Correo = result[0].UserInfo[0].Correo
	u.Nombre = result[0].UserInfo[0].Nombre

	response, _ = json.Marshal(result[0].ToOutput())

	err = deleteToken(u)
	if err != nil {
		http.Error(w, "Error al borrar datos de usuario del cache", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(response)

	return true, http.StatusOK, nil
}
