package strategy

import (
	"auth/redis"
	mssql "auth/singletonmssql"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
)

type Refresh struct{}

func (u *Refresh) Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error) {
	tokenParts := strings.Split(r.Header.Get("Authorization"), " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		http.Error(w, "Formato de token incorrecto", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Obtener el token JWT:
	tokenString := tokenParts[1]
	// Parsear y validar el token JWT
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return getKey(), nil
	})
	if err != nil || !token.Valid {
		http.Error(w, "Token JWT inválido", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Si todo está bien, responder con éxito
	expirationTime := time.Now().Add(72 * time.Hour)
	claims.ExpiresAt = expirationTime.Unix()
	claims.IssuedAt = time.Now().Unix()
	jsonResponse, _ := json.Marshal(claims)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
	redis.RefreshRegis(0, claims.Storage_Key, 72*time.Hour)

	// Actualización de datos
	var dbUserData []UserInfoInput = make([]UserInfoInput, 0)
	user, err := mssql.DoQuery("SELECT id, nombre, nick_name, password, celular, correo, rol, acciones, owner FROM [view_login_auth] WHERE correo = @Correo", []mssql.SqlArgs{
		{Name: "Correo", Value: claims.Storage_Key},
	})
	if err != nil {
		http.Error(w, "Error al conectar el servidor con la base de datos", http.StatusBadRequest)
		return false, http.StatusBadRequest, err
	}

	json.Unmarshal(user, &dbUserData)

	if ok, err := setUserFromCache(1, dbUserData[0].Nombre, user); !ok || err != nil {
		fmt.Println("No se pudo guardar en cache: ", dbUserData[0].Nombre)
	}

	if ok, err := setUserFromCache(2, dbUserData[0].Correo, user); !ok || err != nil {
		fmt.Println("No se pudo guardar en cache: ", dbUserData[0].Correo)
	}

	if ok, err := setUserFromCache(3, dbUserData[0].Celular, user); !ok || err != nil {
		fmt.Println("No se pudo guardar en cache: ", dbUserData[0].Celular)
	}

	return true, http.StatusOK, nil
}
