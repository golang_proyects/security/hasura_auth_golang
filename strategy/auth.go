package strategy

import (
	"auth/redis"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"
	"unicode/utf16"

	"github.com/golang-jwt/jwt"
	"github.com/sirupsen/logrus"
)

type HasuraClaims struct {
	XHasuraRole    string `json:"X-Hasura-Role"`
	XHasuraIsOwner string `json:"X-Hasura-Is-Owner"`
	XHasuraUserID  string `json:"X-Hasura-User-Id"`
}

type Claims struct {
	HTTPSHasuraIoJwtClaims HasuraClaims `json:"https://hasura.io/jwt/claims"`
	jwt.StandardClaims
	Storage_Key string `json:"storage_Key"`
}

type Accion struct {
	Id     int    `json:"id"`
	Nombre string `json:"nombre"`
}

type UserInfoInput struct {
	Id       int      `json:"id"`
	Nombre   string   `json:"nombre"`
	Password string   `json:"password"`
	NickName string   `json:"nick_name"`
	Celular  string   `json:"celular"`
	Correo   string   `json:"correo"`
	Rol      string   `json:"rol"`
	Owner    string   `json:"owner"`
	Acciones []Accion `json:"acciones"`
}

func (u *UserInfoInput) GetCorreo() string {
	return u.Correo
}

func (u *UserInfoInput) GetNombre() string {
	return u.Nombre
}

func (u *UserInfoInput) GetCelular() string {
	return u.Celular
}

type UserInfoOutput struct {
	Id       int      `json:"id"`
	Nombre   string   `json:"nombre"`
	NickName string   `json:"nick_name"`
	Celular  string   `json:"celular"`
	Correo   string   `json:"correo"`
	Rol      string   `json:"rol"`
	Owner    string   `json:"owner"`
	Acciones []Accion `json:"acciones"`
}

func (ui *UserInfoInput) ToOutput() UserInfoOutput {
	return UserInfoOutput{
		Id:       ui.Id,
		Nombre:   ui.Nombre,
		NickName: ui.NickName,
		Celular:  ui.Celular,
		Correo:   ui.Correo,
		Rol:      ui.Rol,
		Owner:    ui.Owner,
		Acciones: ui.Acciones,
	}
}

type LoginResponse struct {
	Token          string         `json:"token"`
	UserInfoOutput UserInfoOutput `json:"data"`
}

// RequestData representa la estructura de los datos de la solicitud.
type RequestData struct {
	Query    string `json:"query"`
	Fragment string `json:"fragment"`
	// Agrega otros campos de la solicitud según sea necesario...
}

// Headers representa la estructura de los encabezados de la solicitud HTTP.
type Headers struct {
	Accept          string `json:"Accept"`
	AcceptEncoding  string `json:"Accept-Encoding"`
	Accept_Language string `json:"Accept-Language"`
	Authorization   string `json:"Authorization"`
	// Agrega otros campos de los encabezados según sea necesario...
}

// SolicitudHTTP representa la estructura completa de la solicitud HTTP.
type SolicitudHTTP struct {
	Headers Headers     `json:"headers"`
	Request RequestData `json:"request"`
}

type UserInfo struct {
	Correo  string `json:"correo"`
	Celular string `json:"celular"`
	Nombre  string `json:"nombre"`
}

type MssqlResponseInfo struct {
	Id           int        `json:"id"`
	Affects_rows int        `json:"affects_rows"`
	Error        string     `json:"error"`
	UserInfo     []UserInfo `json:"info"`
}

type MssqlResponse struct {
	Id           int    `json:"id"`
	Affects_rows int    `json:"affects_rows"`
	Error        string `json:"error"`
}

func (u *MssqlResponseInfo) ToOutput() MssqlResponse {
	return MssqlResponse{
		Id:           u.Id,
		Affects_rows: u.Affects_rows,
		Error:        u.Error,
	}
}

// Interface para todas las estrategias de autenticación
type AuthStrategy interface {
	Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error)
}

type Token interface {
	GetCorreo() string
	GetNombre() string
	GetCelular() string
}

// Esta funcion se implementa para en un futuro implementar una funcion de cambio horario del token
func getKey() []byte {
	return []byte(os.Getenv("AUTH_KEY"))
}

func validToken(key string) (bool, error) {
	if tokenString, err := redis.GetRegis(0, key); tokenString == "" || err != nil {
		return false, err
	} else {
		return true, nil
	}
}

func getUserFromCache(db int, key string) (user []UserInfoInput, err error) {
	var retrievedData []byte
	// Retrieve JSON data from Redis
	retrievedData, err = redis.GetRegisJson(db, key)
	if err != nil {
		return
	}
	if string(retrievedData) == "[]" {
		err = fmt.Errorf("Sin informacion en cache: ", key)
		return
	}
	err = json.Unmarshal(retrievedData, &user)
	if err != nil {
		return
	}
	return

}

func setUserFromCache(db int, key string, val []byte) (ok bool, err error) {
	ok, err = redis.SetRegisJson(db, key, val, time.Hour*72)
	if err != nil || !ok {
		fmt.Println("Error retrieving JSON from Redis:", err)
		return
	}
	return

}

func createToken(user UserInfoInput) (string, error) {
	tokenString, _ := redis.GetRegis(0, user.Correo)
	if tokenString != "" {
		return fmt.Sprintf("%v", tokenString), nil
	} else {
		// Generar el token JWT
		expirationTime := time.Now().Add(72 * time.Hour)
		claims := &Claims{
			HTTPSHasuraIoJwtClaims: HasuraClaims{
				XHasuraRole:    string(user.Rol),
				XHasuraIsOwner: string(user.Owner),
				XHasuraUserID:  fmt.Sprintf("%d", user.Id),
			},
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: expirationTime.Unix(),
				IssuedAt:  time.Now().Unix(),
			},
			Storage_Key: user.Correo,
		}
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
		tokenString, err := token.SignedString(getKey())
		if err != nil {
			return "", nil
		}
		redis.SetRegis(0, user.Correo, tokenString, 3*24*time.Hour)
		return tokenString, nil
	}
}

func deleteToken(token Token) (err error) {
	err = redis.DelRegis(0, token.GetCorreo())
	if err != nil {
		return
	}
	err = redis.DelRegis(1, token.GetNombre())
	if err != nil {
		return
	}
	err = redis.DelRegis(2, token.GetCorreo())
	if err != nil {
		return
	}
	err = redis.DelRegis(3, token.GetCelular())
	if err != nil {
		return
	}
	return
}

func encryptPass(pass string) string {
	salt := os.Getenv("AUTH_ENCRYPT_KEY")

	// Convertir la cadena a bytes usando la codificación UTF-16 little-endian
	utf16Bytes := utf16.Encode([]rune(pass + salt))
	bytes := make([]byte, len(utf16Bytes)*2)
	for i, v := range utf16Bytes {
		bytes[i*2] = byte(v)
		bytes[i*2+1] = byte(v >> 8)
	}

	// Calcular el hash SHA-256
	hash := sha256.Sum256(bytes)

	// Convertir el hash a una cadena hexadecimal
	hashString := strings.ToUpper(hex.EncodeToString(hash[:]))

	return hashString
}

func LoginHandler(w http.ResponseWriter, r *http.Request) {
	var authStrategy AuthStrategy
	if r.URL.Path == "/user" {
		authStrategy = &UsernameAuth{}
	} else if r.URL.Path == "/email" {
		authStrategy = &EmailAuth{}
	} else if r.URL.Path == "/phone" {
		authStrategy = &PhoneAuth{}
	} else if r.URL.Path == "/hook" {
		authStrategy = &HookAuth{}
	} else if r.URL.Path == "/valid" {
		authStrategy = &Valid{}
	} else if r.URL.Path == "/refresh" {
		authStrategy = &Refresh{}
	} else if r.URL.Path == "/token" {
		authStrategy = &TokenInfo{}
	} else if r.URL.Path == "/recovery" {
		authStrategy = &Recovery{}
	} else if r.URL.Path == "/changepwd" {
		authStrategy = &ChangePwd{}
	} else {
		http.Error(w, "Estrategia de autenticación no válida", http.StatusBadRequest)
		return
	}
	_, status, _ := authStrategy.Authenticate(w, r)

	logrus.WithFields(logrus.Fields{
		"URL":    r.URL.Path,
		"status": status,
		"ip":     getClientIP(r),
	}).Info("New Request")
}

func getClientIP(r *http.Request) string {
	// Intenta obtener la dirección IP del campo RemoteAddr
	remoteAddr := strings.TrimSpace(r.RemoteAddr)
	if strings.Contains(remoteAddr, "[") && strings.Contains(remoteAddr, "]") {
		// Si el campo RemoteAddr contiene una dirección IPv6 entre corchetes, extrae solo la dirección IP
		remoteAddr = strings.Split(remoteAddr, "]")[0]
		remoteAddr = strings.TrimPrefix(remoteAddr, "[")
	}
	// La dirección IP limpia se devuelve
	return remoteAddr
}
