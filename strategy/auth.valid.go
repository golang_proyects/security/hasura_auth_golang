package strategy

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/golang-jwt/jwt"
)

type Valid struct{}

func (u *Valid) Authenticate(w http.ResponseWriter, r *http.Request) (bool, int, error) {
	tokenParts := strings.Split(r.Header.Get("Authorization"), " ")
	if len(tokenParts) != 2 || tokenParts[0] != "Bearer" {
		http.Error(w, "Formato de token incorrecto", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Obtener el token JWT:
	tokenString := tokenParts[1]
	// Parsear y validar el token JWT
	claims := &Claims{}
	token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return getKey(), nil
	})
	if err != nil || !token.Valid {
		http.Error(w, "Token JWT inválido", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}
	// Verificar las reclamaciones específicas de Hasura
	hasuraClaims := claims.HTTPSHasuraIoJwtClaims

	if hasuraClaims.XHasuraUserID != "5" {
		http.Error(w, "Usuario no autorizado", http.StatusUnauthorized)
		return false, http.StatusUnauthorized, nil
	}

	// Si todo está bien, responder con éxito
	jsonResponse, _ := json.Marshal(hasuraClaims)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
	return true, http.StatusOK, nil
}
