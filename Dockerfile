FROM golang:1.21.9-alpine3.18 AS base

WORKDIR /app
ADD . .
RUN apk update && apk add build-base git
RUN go mod download
RUN CGO_ENABLED=0 go build -ldflags "-s -w" -o main .

FROM scratch AS production

COPY --from=base /app/main ./main
COPY --from=base /app/src ./src
CMD ["./main"]
