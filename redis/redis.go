package redis

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/redis/go-redis/v9"
)

type map_clients map[int]*redis.Client

var ctx context.Context

var aux map_clients = make(map_clients)
var Rdb *map_clients = &aux
var lock = &sync.Mutex{}

func getInstanceRedis(db int) (*map_clients, error) {
	ctx = context.Background()

	red := *Rdb

	if _, ok := red[db]; ok != true {
		lock.Lock()
		defer lock.Unlock()
		if _, ok := red[db]; ok != true {
			// var DB int
			// DB, _ = strconv.Atoi(os.Getenv("REDIS_DB"))
			var POOL_SIZE int
			POOL_SIZE, _ = strconv.Atoi(os.Getenv("REDIS_POOL_SIZE"))
			red[db] = redis.NewClient(&redis.Options{
				Addr: fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
				// Username: os.Getenv("REDIS_USER"),
				Password: os.Getenv("REDIS_PASSWORD"),
				DB:       db,
				PoolSize: POOL_SIZE,
			})
			if err := red[db].Ping(ctx).Err(); err != nil {
				fmt.Println("connect: cannot get redis conn singleton:", err.Error())
				return nil, err
			}
		}
	}
	return Rdb, nil

}

func SetRegis(db int, key string, value interface{}, timeTiLive time.Duration) (err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		if err = red[db].Set(ctx, key, fmt.Sprintf("%v", value), timeTiLive).Err(); err != nil {
			fmt.Println("setRegister: cannot set redis:", err.Error())
		}

	}
	return
}

func GetRegis(db int, key string) (val interface{}, err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		return red[db].Get(ctx, key).Result()
	} else {
		return nil, err
	}
}

func SetRegisJson(db int, key string, val []byte, timeToLive time.Duration) (ok bool, err error) {
	ok = true
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		err = red[db].Set(ctx, key, val, timeToLive).Err()
		if err != nil {
			ok = false
			return
		}
		return
	} else {
		return false, err
	}
}

func GetRegisJson(db int, key string) (val []byte, err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		return red[db].Get(ctx, key).Bytes()
	} else {
		return nil, err
	}
}

func DelRegis(db int, key string) (err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		red[db].Del(ctx, key)
	} else {
		fmt.Println("setRegister: cannot del redis:", err.Error())
	}
	return
}

func RefreshRegis(db int, key string, timeTiLive time.Duration) (err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		red[db].Expire(ctx, key, timeTiLive)
	} else {
		fmt.Println("setRegister: cannot del redis:", err.Error())
	}
	return
}

func AAddRQueue(db int, key string, val ...string) (ok bool, err error) {

	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		if data := red[db].RPush(ctx, key, val); data.Val() > 0 {
			// if data := rdb.LPush(ctx, key, val); data.Val() > 0 {
			ok = true
		} else {
			// fmt.Printf("%#v", data)
			ok = false
		}
	} else {
		fmt.Println("setRegister: cannot get redis:", err.Error())
	}

	return
}

func AAddQueue(db int, key string, val ...string) (ok bool, err error) {

	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		if data := red[db].LPush(ctx, key, val); data.Val() > 0 {
			// if data := rdb.LPush(ctx, key, val); data.Val() > 0 {
			ok = true
		} else {
			// fmt.Printf("%#v", data)
			ok = false
		}
	} else {
		fmt.Println("setRegister: cannot get redis:", err.Error())
	}

	return
}

func popQueue(db int, key string) (obj interface{}, err error) {

	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		data := red[db].LPop(ctx, key)
		str := strings.Replace(fmt.Sprintf("%v", data.Val()), "} ", "},", len(data.Val()))
		fmt.Println(str)
		err := json.Unmarshal([]byte(str), &obj)
		if err != nil {
			fmt.Println("error unmarshal:", err.Error())
		}
	} else {
		fmt.Println("setRegister: cannot get redis:", err.Error())
	}

	return

}

func popQueueString(db int, key string) (data string, err error) {
	if Rdb, err = getInstanceRedis(db); err == nil {
		red := *Rdb
		data = red[db].LPop(ctx, key).Val()
	} else {
		fmt.Println("setRegister: cannot get redis:", err.Error())
	}

	return

}
